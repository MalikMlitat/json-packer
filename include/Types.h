/**

    @file Types.h
    @brief defines needed types to perform read/write operations on JSON data
*/
#ifndef TYPES_H
#define TYPES_H

#include <spdlog/spdlog.h>

#include <map>
#include <string>
#include <variant>

#include <nlohmann/json.hpp>

/**
 * @typedef json
 * @brief Alias for nlohmann::json.
 */
using json = nlohmann::json;

/**
 * @typedef json_key_integer_map_t
 * @brief Map between JSON keys and integer ids.
 */
using json_key_integer_map_t = std::map<std::string, uint32_t>;
/**
 * @typedef json_int_values_map_t
 * @brief Map between integer ids and JSON values.
 */
using json_int_values_map_t = std::map<uint32_t, std::variant<int, unsigned int, float, bool, std::string>>;

#endif  // TYPES_H
