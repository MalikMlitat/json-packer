/**

    @file DictionaryBuilder.h
    @brief Header file for the DictionaryBuilder class, which reads a JSON file and builds a dictionary of 
           unique string values and their integer representations, along with a map of the JSON values to 
           their corresponding integer keys.
*/

#ifndef _DICTIONARY_BUILDER_H_
#define _DICTIONARY_BUILDER_H_

#include <string>
#include "Types.h"

/**

    @class DictionaryBuilder

    @brief The DictionaryBuilder class builds a dictionary of unique string values and their integer representations, along with a map of the JSON values to 
           their corresponding integer keys.
*/

class DictionaryBuilder {
 public:
  /**
   * Constructs a new instance of the DictionaryBuilder class with the specified input file.
   * @param input_file The path to the input JSON file.
   */
  explicit DictionaryBuilder(const std::string &input_file);

  /**
   * @brief Logs/prints the json int values map.
   * 
   * @param values_mapper The json int values map to be printed.
   */
  void print_json_int_values_map(const json_int_values_map_t &values_mapper);

  /**
   * @brief Prints the json int values map (m_json_value_map).
   * 
   */
  void print_json_int_values_map();

  /**
   * @brief Getter method to get the JSON value map.
   *
   * @return The JSON value map.
   */
  inline const json_int_values_map_t& get_json_value_map() const {
      return m_json_value_map;
  }

  /**
   * @brief Getter method to get the JSON key integer map.
   *
   * @return The JSON key integer map.
   */
  inline const json_key_integer_map_t& get_json_key_int_map() const {
      return m_json_key_int_map;
  }

 private:
   /**
   * Builds a dictionary of unique string values and their integer representations, along with a map of the JSON values
   * to their corresponding integer values.
   */
  void build();

 private:
  const std::string      m_input_file;  // The path to the input JSON file.
  json_int_values_map_t  m_json_value_map;
  json_key_integer_map_t m_json_key_int_map;
};

#endif  // _DICTIONARY_BUILDER_H_
