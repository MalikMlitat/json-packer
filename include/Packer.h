/**

    @file Packer.h
    @brief Header file for the Packer class, which performs packing and unpacking of json data.

*/

#ifndef _PACKER_H_
#define _PACKER_H_

#include "Types.h"
#include "DictionaryBuilder.h"
#include <string>

/**

    @class Packer

    @brief The Packer class is responsible for packing and unpacking data using TVL coding.
*/

class Packer {
 public:
    /**
    * @brief Constructor for the Packer class.
    *
    * @param json_input_file The path of the json input file to be packed.
    * @param keys_output_file The path of the output file to dump int-key pairs map.
    * @param values_output_file The path of the output file to dump the int-values pair map.
    */
    Packer(const std::string& json_input_file,
           const std::string& keys_output_file,
           const std::string& values_output_file);

    /**
     * @brief Packs the data from the input file and writes it to the output files.
     */
    void pack();

    /**
     * @brief Dumps the key-to-integer mapping dictionary to output file in TVL format.
     * 
     * @param key_integer_map The output key-to-integer mapping dictionary.
     */
    void dump_key_integer_map_to_output_file_using_TVL(const json_key_integer_map_t& key_integer_map);

    /**
     * @brief Reads the key-to-integer mapping dictionary from a file in TVL format.
     * 
     * @param deserlized_dict The key-to-integer mapping dictionary results from the output file.
     */
    // NOLINTNEXTLINE(runtime/references)
    void deserialize_key_integer_map_from_output_file_using_TVL(json_key_integer_map_t& deserlized_dict);

    /**
     * @brief Dumps the JSON values to the output file in TVL format.
     * 
     * @param values The JSON int-values map to be written to foutput ile.
     */
    void dump_int_values_map_to_output_file_using_TVL(const json_int_values_map_t& values);

    /**
     * @brief deserialize the JSON values from the values_output_file in TVL format.
     * 
     * @param values The JSON int-values mapt results from the output file.
     */
    // NOLINTNEXTLINE(runtime/references)
    void deserialize_int_values_map_from_output_file_using_TVL(json_int_values_map_t& values);

    /**
     * @brief returns m_dictionary_builder object
     *
     */
    inline DictionaryBuilder* get_dectionary_builder() { return &m_dictionary_builder;}


 private:
    const std::string m_json_input_file;
    const std::string m_keys_output_file;
    const std::string m_values_output_file;
    DictionaryBuilder m_dictionary_builder;
};

#endif  // _PACKER_H_
