.PHONY: clean install-dependencies build test coverage docs cppcheck cpplint

install-dependencies:
    # tools
	apt-get update; apt-get install -y make cmake gcc g++ doxygen graphviz lcov cppcheck python3-pip
    # build deps
	apt-get update; apt-get install -y nlohmann-json3-dev libfmt-dev libgtest-dev libspdlog-dev

build:
	[ -d build ] || mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Debug .. && make -j10

test:
	$(MAKE) build
	cd build && ./unit_tests

docs:
	doxygen Doxyfile

cppcheck:
	cppcheck --enable=all --suppress=multiCondition --error-exitcode=1 ./src main.cpp
cpplint:
	pip3 install cpplint
	cpplint --linelength=120 --filter=-legal/copyright,-build/header_guard ./src/* ./include/*

coverage:
	$(MAKE) test
	cd build
	gcov CMakeFiles/{json_packer,jsonpacker_lib}.dir/*
	lcov  --exclude '*test*' --capture --directory . --output-file build/coverage.info
	lcov  --exclude '*test*' --remove build/coverage.info 'tests' '/usr/*' 'fmt*' --output-file build/coverage.info
	genhtml build/coverage.info --output-directory build/coverage_report

clean:
	rm -rfv ./build ./docs
