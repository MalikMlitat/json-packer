# Project: JSON Packer


## 0. Overview
The JSON Packer is a C++ program that can be used to serialize and deserialize JSON data to/from binary format. It provides a simple and efficient way to store JSON data using TVL encoding.

## 1. Example
input.json:
```json
{ "key1": "firstValue", "key2": 42, "key3": true}
```
program execution:

```
./json_packer input.json key_output.bin values_output.bin
```
The program  encode the input so that the keys are put into a
dictionary, the dictionary maps the keys (strings) to an integer:

```json
{ "key1":1, "key2":2, "key3":3 }
```

The key strings are then replaced with a number; The record is then represented as:

```json
{ 1:firstValue, 2:42, 3:TRUE }
```

Both outputs are dumped to the outputfiles in a binary form using TLV encoding.

## 2. Implementation
The library is implemented in C++, using modern C++ features such as STL containers and variants. It is built on top of the widely-used JSON library [nlohmann/json](https://github.com/nlohmann/json).

For logging the header-only/compiled, C++ logging library [spdlog](https://github.com/gabime/spdlog) is used.


## 3. Dependencies
Build, test and code checkes dependencies are listed in the [Makefile](./Makefile), to install those run:

```
$ sudo make install-dependencies
```

## 4. Building the source code
The following command builds the source code:
```
$ make build
```

## 5. Running tests
Unittests are written using [GoogleTests](https://github.com/google/googletest) framework:
```
$ make test
```

## 6. Quality
Several quality measures are used to increase the quality of the project.

Those measures are manifisted in the gitlab CI/CD in [.gitlab-ci.yml](./.gitlab-ci.yml) using **debian** docker based image as requested:

### 6.1. Static code checks
To perform static code analysis, [cpplint](https://github.com/cpplint/cpplint) and [cppcheck](https://github.com/danmar/cppcheck) are used for this project:
```
$ make cppcheck
$ make cpplint
```
### 6.2 Dynamic code checks
source code is instrumented to enable the [AddressSanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) to detects addressability issues and [LeakSanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizerLeakSanitizer) to detect memory leaks during unittesting.

### 6.3 Generating documentation
Documentation is done using Doxygen:
```
$ make docs
```
This will generate the HTML documentation in the `docs/html` directory incl. dependency and workflow graphs.

HTML report is published to pipeline artifacts.

### 6.4 Code coverage report
gcov is used to report the coverage os the source code, the code is instumented and an HTML report is generated:
```
$ make coverage
```
HTML report is published to pipeline artifacts.

