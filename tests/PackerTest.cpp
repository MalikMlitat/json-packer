#include <gtest/gtest.h>

#include "Packer.h"


TEST(PackerTest, DumpKeyIntMapToOutputFile_deserilizedDumpedKeys_shallBeEqual) {
    // input file is not used since pack() is not used
    Packer packer("../data/test_input.json", "/tmp/output.bin", "/tmp/dict.bin");

    // Set up test data
    json_key_integer_map_t key_int_map = {
        {"key1", 1},
        {"key2", 2},
        {"key3", 3},
        {"key4", 4}
    };

    packer.dump_key_integer_map_to_output_file_using_TVL(key_int_map);

    json_key_integer_map_t deserilized_dict;
    packer.deserialize_key_integer_map_from_output_file_using_TVL(deserilized_dict);
    // Check the values in the dictionary
    EXPECT_EQ(1, deserilized_dict["key1"]);
    EXPECT_EQ(2, deserilized_dict["key2"]);
    EXPECT_EQ(3, deserilized_dict["key3"]);

    EXPECT_EQ(key_int_map, deserilized_dict);
}

TEST(PackerTest, DumpIntValuesMapToOutputFile_deserilizedDumpedValues_shallBeEqual) {
    // input file is not used since pack() is not used
    Packer packer("../data/test_input.json", "/tmp/output.bin", "/tmp/dict.bin");

    // Set up test data
    json_int_values_map_t valuesMapper = {
        {1, std::string("firstValue")},
        {2, 42.2f},
        {3, true},
        {4, -100}
    };

    packer.dump_int_values_map_to_output_file_using_TVL(valuesMapper);

    json_int_values_map_t deserilizedValuesMapper;
    packer.deserialize_int_values_map_from_output_file_using_TVL(deserilizedValuesMapper);

    // Check the values in the values map
    EXPECT_EQ("firstValue", std::get<std::string>(deserilizedValuesMapper[1]));
    EXPECT_EQ(42.2f,        std::get<float>(deserilizedValuesMapper[2]));
    EXPECT_EQ(true,         std::get<bool>(deserilizedValuesMapper[3]));
    EXPECT_EQ(-100,         std::get<signed int>(deserilizedValuesMapper[4]));

    EXPECT_EQ(valuesMapper, deserilizedValuesMapper);
}

TEST(PackerTest, ReadJsonInputFile_with2Rows7Records_allValuesShallPresent) {
    Packer packer("../data/test_input_2_rows.json", "/tmp/output.bin", "/tmp/dict.bin");

    packer.pack();
    packer.get_dectionary_builder()->print_json_int_values_map();

    json_int_values_map_t deserilizedValuesMapper;
    packer.deserialize_int_values_map_from_output_file_using_TVL(deserilizedValuesMapper);

    // Check the values in the values map
    ASSERT_EQ(7,         packer.get_dectionary_builder()->get_json_value_map().size());

    EXPECT_EQ("value",   std::get<std::string>(deserilizedValuesMapper[1]));
    EXPECT_EQ(42,        std::get<unsigned int>(deserilizedValuesMapper[2]));
    EXPECT_EQ(true,      std::get<bool>(deserilizedValuesMapper[3]));
    EXPECT_EQ(3.3f,      std::get<float>(deserilizedValuesMapper[4]));
    EXPECT_EQ("dsewtew", std::get<std::string>(deserilizedValuesMapper[5]));
    EXPECT_EQ(-3221,     std::get<signed int>(deserilizedValuesMapper[6]));
    EXPECT_EQ("kkkkkk",  std::get<std::string>(deserilizedValuesMapper[7]));
}

TEST(PackerTest, ReadJsonInputFile_with2Rows7Records_allKeysShallPresent) {
    Packer packer("../data/test_input_2_rows.json", "/tmp/output.bin", "/tmp/dict.bin");

    packer.pack();

    json_key_integer_map_t deserilized_dict;
    packer.deserialize_key_integer_map_from_output_file_using_TVL(deserilized_dict);
    // Check the values in the dictionary
    ASSERT_EQ(7, packer.get_dectionary_builder()->get_json_key_int_map().size());

    EXPECT_EQ(1, deserilized_dict["key1"]);
    EXPECT_EQ(2, deserilized_dict["key2"]);
    EXPECT_EQ(3, deserilized_dict["key3"]);
    EXPECT_EQ(4, deserilized_dict["key3.3"]);
    EXPECT_EQ(5, deserilized_dict["key4"]);
    EXPECT_EQ(6, deserilized_dict["key5"]);
    EXPECT_EQ(7, deserilized_dict["key6"]);
}
