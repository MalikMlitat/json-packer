#include <gtest/gtest.h>

#include "DictionaryBuilder.h"

TEST(DictionaryBuilderTest, JsonInputFileContains3ValidRecords_valuesMapAndKeyMap_shouldInclude3Elements)
{
    // Create an instance of DictionaryBuilder
    DictionaryBuilder builder("../data/test_input.json");

    json_int_values_map_t  values_mapper =  builder.get_json_value_map();
    json_key_integer_map_t dictionary = builder.get_json_key_int_map();

    // Check the values in the dictionary
    ASSERT_EQ(3, dictionary.size());
    EXPECT_EQ(1, dictionary["key1"]);
    EXPECT_EQ(2, dictionary["key2"]);
    EXPECT_EQ(3, dictionary["key3"]);

    // Check the values in the values map
    ASSERT_EQ(3, values_mapper.size());
    EXPECT_EQ("firstValue", std::get<std::string>(values_mapper[1]) );
    EXPECT_EQ(42,           std::get<unsigned int>(values_mapper[2]) );
    EXPECT_EQ(true,         std::get<bool>(values_mapper[3]) );
}



TEST(DictionaryBuilderTest, JsonContains3RecordsButOnly2Valid_valuesMapAndKeyMap_shouldIncludeOnly2Elements)
{
    DictionaryBuilder builder("../data/test_unsupported_input.json");

    json_int_values_map_t  values_mapper =  builder.get_json_value_map();
    json_key_integer_map_t dictionary = builder.get_json_key_int_map();

    // Check the values in the dictionary
    ASSERT_EQ(2, dictionary.size() );
    EXPECT_EQ(1, dictionary["key1"] );
    EXPECT_EQ(2, dictionary["key2"] );

    // Check the values in the values map
    ASSERT_EQ(2, values_mapper.size() );
    EXPECT_EQ("value", std::get<std::string>(values_mapper[1]) );
    EXPECT_EQ(42,      std::get<unsigned int>(values_mapper[2]) );
}

TEST(DictionaryBuilderTest, JsonInputFileNotExist_KeyIntMapAndJsonValueMap_areEmpty)
{
    // Create an instance of DictionaryBuilder
    DictionaryBuilder builder("file_is_not_present.json");

    // Check that the dictionary and values map are empty
    EXPECT_TRUE(builder.get_json_key_int_map().empty());
    EXPECT_TRUE(builder.get_json_value_map().empty());
}

