/**
 * @file main.cpp
 * @brief Contains the main function to run the JSON packer program.
 * The program reads a JSON input file, extracts the keys and values,
 * and writes them to separate output files in a packed binary format.
 * 
 */

#include "spdlog/spdlog.h"
#include "Packer.h"
#include <iostream>

/**
 * @brief Entry point for the JSON packer program.
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 * @return 0 if the program executed successfully, 1 otherwise.
 */

int main(int argc, char *argv[]) {
  if (argc != 4) {
    spdlog::error(
        "Usage: ./json_packer <json_input_file> <keys_output_file> <values_output_file>");
    return 1;
  }

  try {
    Packer packer(argv[1], argv[2], argv[3]);
    packer.pack();
  } catch (const std::exception &ex) {
    std::cerr << "Error: " << ex.what() << std::endl;
    return 1;
  }

  return 0;
}
