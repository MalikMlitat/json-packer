#include "DictionaryBuilder.h"
#include <fstream>
#include <iostream>

DictionaryBuilder::DictionaryBuilder(const std::string &input_file) : m_input_file(input_file) {
    this->build();
}


void DictionaryBuilder::build() {
    spdlog::info("DictionaryBuilder::{} Building json_values_mapper and string_integer_map ...", __func__);

    std::ifstream input(m_input_file);
    if (!input.is_open()) {
        spdlog::error("{} File does not exists!", m_input_file);
        return;
    }
    std::string line;
    uint32_t jsonIndex = 1;
    while (std::getline(input, line)) {
        json record = json::parse(line);
        for (auto &kvp : record.items()) {
            const std::string &key = kvp.key();
            uint32_t id = m_json_key_int_map.size() + 1;  // start by 1

            switch (kvp.value().type()) {
                case json::value_t::string:
                {
                    const std::string &value = kvp.value();
                    m_json_value_map[jsonIndex++] = reinterpret_cast<const char *>(value.c_str());
                    m_json_key_int_map[key] = id;
                    break;
                }
                case json::value_t::boolean:
                {
                    bool value = kvp.value();
                    m_json_value_map[jsonIndex++] = value;
                    m_json_key_int_map[key] = id;
                    break;
                }
                case json::value_t::number_integer:
                {
                    int value = kvp.value();
                    m_json_value_map[jsonIndex++] = value;
                    m_json_key_int_map[key] = id;
                    break;
                }
                case json::value_t::number_unsigned:
                {
                    unsigned int value = kvp.value();
                    m_json_value_map[jsonIndex++] = value;
                    m_json_key_int_map[key] = id;
                    break;
                }
                case json::value_t::number_float:
                {
                    float value = kvp.value();
                    m_json_value_map[jsonIndex++] = value;
                    m_json_key_int_map[key] = id;
                    break;
                }
                default:
                    spdlog::warn("Unsupported JSON value type name: {},value: {}", kvp.value().type_name(),
                                                                                   kvp.value().dump());
                    break;
                }
        }
    }  // end while
    spdlog::info("DictionaryBuilder::{} {} records found ", __func__, m_json_key_int_map.size());
    for (const auto& [key, value] : m_json_key_int_map) {
        spdlog::debug("DictionaryBuilder::{} dictionary elements: {}: {}", __func__, key, value);
    }
}

void DictionaryBuilder::print_json_int_values_map() {
    this->print_json_int_values_map(m_json_value_map);
}


void DictionaryBuilder::print_json_int_values_map(const json_int_values_map_t &values_mapper) {
  for (const auto &[key, value] : values_mapper) {
    std::visit(
        [](const auto &val) {
          using T = std::decay_t<decltype(val)>;
          if constexpr (std::is_same_v<T, int>) {  // cppcheck-suppress multiCondition
            spdlog::info("Packer::print_values_mapper: integer value: {} ", val);
          } else if constexpr (std::is_same_v<T, unsigned int>) {  // cppcheck-suppress multiCondition
            spdlog::info("Packer::print_values_mapper: unsigned integer value:: {} ", val);
          } else if constexpr (std::is_same_v<T, float>) {
            spdlog::info("Packer::print_values_mapper: float value: {} ", val);
          } else if constexpr (std::is_same_v<T, bool>) {
            spdlog::info("Packer::print_values_mapper: bool value: {}", val);
          } else if constexpr (std::is_same_v<T, std::string>) {
            spdlog::info("Packer::print_values_mapper: string value: {} ", val);
          }
        },
        value);
  }
}
