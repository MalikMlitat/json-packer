#include "Packer.h"
#include "DictionaryBuilder.h"

#include <fstream>
#include <iostream>

Packer::Packer(const std::string &json_input_file,
               const std::string &keys_output_file,
               const std::string &values_output_file)
    : m_json_input_file(json_input_file),
      m_keys_output_file(keys_output_file),
      m_values_output_file(values_output_file),
      m_dictionary_builder(json_input_file) {

  spdlog::info("Packer::{} is started...", __func__);
  spdlog::info("Input args are:\n json file: {0}\n keys_output_file: {1}\n values_output_file: {2}",
                m_json_input_file, m_keys_output_file, m_values_output_file);
}

void Packer::pack() {
  spdlog::info("Packer::{}...", __func__);
  // dumps {"key1":1,"key2":2"...} to key_output file
  dump_key_integer_map_to_output_file_using_TVL(m_dictionary_builder.get_json_key_int_map());
  // dupms {1:"value", 2:10, 3:true} to values output file
  dump_int_values_map_to_output_file_using_TVL(m_dictionary_builder.get_json_value_map());
  spdlog::info("Packer::{} done", __func__);
}

void Packer::dump_key_integer_map_to_output_file_using_TVL(const json_key_integer_map_t &dictionary) {
  spdlog::debug("Packer::{}...", __func__);

  std::ofstream dict_out(m_keys_output_file, std::ios::binary);

  // Write the number of entries in the dictionary as a 4-byte integer (Type=0)
  uint32_t num_entries = dictionary.size();
  uint8_t type = 0;
  uint32_t len = sizeof(num_entries);
  dict_out.write(reinterpret_cast<const char *>(&type), sizeof(type));
  dict_out.write(reinterpret_cast<const char *>(&len), sizeof(len));
  dict_out.write(reinterpret_cast<const char *>(&num_entries),
                 sizeof(num_entries));

  // Write each key-value pair in the dictionary
  for (const auto &kvp : dictionary) {
    const std::string &key = kvp.first;
    uint32_t id = kvp.second;
    spdlog::debug("Packer::{__func__} key:{key} id: {id}", key, id);
    uint32_t key_len = key.size();

    // Write the key ID as a 4-byte integer (Type=1)
    type = 1;
    len = sizeof(id);
    dict_out.write(reinterpret_cast<const char *>(&type), sizeof(type));
    dict_out.write(reinterpret_cast<const char *>(&len), sizeof(len));
    dict_out.write(reinterpret_cast<const char *>(&id), sizeof(id));

    // Write the key length as a 4-byte integer (Type=2)
    type = 2;
    len = sizeof(key_len);
    dict_out.write(reinterpret_cast<const char *>(&type), sizeof(type));
    dict_out.write(reinterpret_cast<const char *>(&len), sizeof(len));
    dict_out.write(reinterpret_cast<const char *>(&key_len), sizeof(key_len));

    // Write the key string as a sequence of bytes (Type=3)
    type = 3;
    len = key_len;
    dict_out.write(reinterpret_cast<const char *>(&type), sizeof(type));
    dict_out.write(reinterpret_cast<const char *>(&len), sizeof(len));
    dict_out.write(key.c_str(), len);

    // Write the key string length as a 4-byte integer (Type=4)
    type = 4;
    len = sizeof(key_len);
    dict_out.write(reinterpret_cast<const char *>(&type), sizeof(type));
    dict_out.write(reinterpret_cast<const char *>(&len), sizeof(len));
    dict_out.write(reinterpret_cast<const char *>(&key_len), sizeof(key_len));
  }

  dict_out.close();
  spdlog::debug("Packer::{} done", __func__);
}

void Packer::deserialize_key_integer_map_from_output_file_using_TVL(json_key_integer_map_t &deserlized_dict) {
  spdlog::debug("Packer::{}...", __func__);

  std::ifstream dict_in(m_keys_output_file, std::ios::binary);
  if (!dict_in.is_open()) {
    spdlog::error("{} File does not exist!", m_keys_output_file);
    return;
  }

  // Read the number of entries in the dictionary
  uint32_t num_entries;
  uint8_t type;
  uint32_t len;
  dict_in.read(reinterpret_cast<char *>(&type), sizeof(type));
  dict_in.read(reinterpret_cast<char *>(&len), sizeof(len));
  dict_in.read(reinterpret_cast<char *>(&num_entries), sizeof(num_entries));
  spdlog::debug("Packer::{__func__}: num_entries size: {}", __func__, num_entries);

  // Loop through each TLV-encoded data in the file and add to the dictionary
  std::string key;
  uint32_t id;

  for (int i = 0; i < num_entries; i++) {
    while (dict_in.read(reinterpret_cast<char *>(&type), sizeof(type))) {
      dict_in.read(reinterpret_cast<char *>(&len), sizeof(len));

      if (type == 1) {
        dict_in.read(reinterpret_cast<char *>(&id), sizeof(id));
      } else if (type == 2 || type == 4) {
        dict_in.ignore(len);
      } else if (type == 3) {
        key.resize(len);
        dict_in.read(&key[0], len);
        // Add the key-value pair to the dictionary
        deserlized_dict[key] = id;
        spdlog::debug("Packer::{__func__}: Add the key-value pair "
                      "{key}:{id} to the dictionary",
                      __func__, key, id);
      }
    }
  }

  dict_in.close();
  spdlog::debug("Packer::{} done", __func__);
}

void Packer::dump_int_values_map_to_output_file_using_TVL(const json_int_values_map_t &values) {
  spdlog::debug("Packer::{}...", __func__);
  std::ofstream output(m_values_output_file, std::ios::binary);
  for (const auto &[key, val] : values) {
    // Write key ID
    output.write(reinterpret_cast<const char *>(&key), sizeof(key));

    // Write value type ID
    uint8_t type_id;
    if (val.index() == 0)
      type_id = 1;  // int
    else if (val.index() == 1)
      type_id = 2;  // unsigned int
    else if (val.index() == 2)
      type_id = 3;  // float
    else if (val.index() == 3)
      type_id = 4;  // bool
    else if (val.index() == 4)
      type_id = 5;  // std::string
    output.write(reinterpret_cast<const char *>(&type_id), sizeof(type_id));

    // Write value
    switch (val.index()) {
    case 0: {  // int
      int32_t value = std::get<int>(val);
      output.write(reinterpret_cast<const char *>(&value), sizeof(value));
      break;
    }
    case 1: {  // unsigned int
      uint32_t value = std::get<unsigned int>(val);
      output.write(reinterpret_cast<const char *>(&value), sizeof(value));
      break;
    }
    case 2: {  // float
      float value = std::get<float>(val);
      output.write(reinterpret_cast<const char *>(&value), sizeof(value));
      break;
    }
    case 3: {  // bool
      bool value = std::get<bool>(val);
      output.write(reinterpret_cast<const char *>(&value), sizeof(value));
      break;
    }
    case 4: {  // std::string
      const std::string &value = std::get<std::string>(val);
      uint32_t len = value.size();
      output.write(reinterpret_cast<const char *>(&len), sizeof(len));
      output.write(value.c_str(), len);
      break;
    }
    default:
      spdlog::warn("Unsupported variant type: {}", val.index());
      break;
    }
  }
  output.close();
  spdlog::debug("Packer::{} done", __func__);
}

void Packer::deserialize_int_values_map_from_output_file_using_TVL(json_int_values_map_t &values) {
  spdlog::debug("Packer::{}...", __func__);

  std::ifstream input(m_values_output_file, std::ios::binary);
  if (!input) {
    spdlog::error("Error opening input file");
    return;
  }

  uint32_t key;
  uint8_t type_id;
  while (input.read(reinterpret_cast<char *>(&key), sizeof(key)) &&
         input.read(reinterpret_cast<char *>(&type_id), sizeof(type_id))) {
    if (type_id == 1) {  // int
      int32_t value;
      if (input.read(reinterpret_cast<char *>(&value), sizeof(value))) {
        values[key] = value;
      } else {
        spdlog::error("Error reading int value for key {}", key);
        break;
      }
    } else if (type_id == 2) {  // unsigned int
      uint32_t value;
      if (input.read(reinterpret_cast<char *>(&value), sizeof(value))) {
        values[key] = value;
      } else {
        spdlog::error("Error reading unsigned int value for key {}", key);
        break;
      }
    } else if (type_id == 3) {  // float
      float value;
      if (input.read(reinterpret_cast<char *>(&value), sizeof(value))) {
        values[key] = value;
      } else {
        spdlog::error("Error reading float value for key {}", key);
        break;
      }
    } else if (type_id == 4) {  // bool
      bool value;
      if (input.read(reinterpret_cast<char *>(&value), sizeof(value))) {
        values[key] = value;
      } else {
        spdlog::error("Error reading bool value for key {}", key);
        break;
      }
    } else if (type_id == 5) {  // std::string
      uint32_t len;
      if (input.read(reinterpret_cast<char *>(&len), sizeof(len))) {
        std::vector<char> buffer(len);
        if (input.read(buffer.data(), len)) {
          values[key] = std::string(buffer.data(), len);
        } else {
          spdlog::error("Error reading string value for key {}", key);
          break;
        }
      } else {
        spdlog::error("Error reading string length for key {}", key);
        break;
      }
    } else {
      spdlog::error("Unsupported value type ID: {}", static_cast<int>(type_id));
      break;
    }
  }

  input.close();
  spdlog::debug("Packer::{} done", __func__);
}
